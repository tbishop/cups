#!/bin/bash
#
# Usage:
#	run the script (with sudo) to install all shared printers
#

function enter_yesno {
	while true ; do
		echo -n "${1} (Y/N)? " 1>&2
 		read reply
		case "$reply" in
		    Y*|y*) return 0 ;;
		    N*|n*) return 1 ;;
		esac
		echo "Invalid input, try again ..." 1>&2
	done
}

function exit_handler {
	# Upon rollback we first delete everything from the rollback-delete-list and
	# then extract everything from the rollback-tar.
	# Additionally installed Debian packages are not reverted.
	if [ -r "${RBDEL}" ] ; then
		echo
		echo "Premature termination: removing any modified files/folders"
		while read F ; do
			echo "${F}"
			rm -rf "${F}"
		done < "${RBDEL}"
		if [ -r "${RBTAR}" ] ; then
			echo
			echo "Premature termination: restoring original versions of modified files/folders"
			if tar --extract -C / --verbose --atime-preserve --preserve-permissions --absolute-names -f "${RBTAR}" ; then
				echo
				echo "Changes successfully reverted."
				echo "You can now run the postinstall-script again."
			else
				echo "*** Could not revert changes made during the post-install!"
				echo "*** Re-running the post-install script IS NOT SAFE, you may have to reinstall the system!"
			fi
		fi
	else
		echo
		echo "Premature termination: no changes were made to the system made so far."
		echo "You can now run the postinstall-script again."
	fi
	rm -rf "${WORKDIR}"
}

function die {
	# die
	# die "command"
	# die "" "error message"

	echo
	if [ $# -gt 1 ] ; then
		echo "*** Error: $2 ***"
	elif [ -z "$1" ] ; then
		echo "*** An error has occurred, terminating ***"
	else
		echo "*** Error executing '${1}', terminating ***"
	fi
	# Will call an exit handler
	exit 1
}

############################################################################################
########################################## MAIN ############################################
############################################################################################

if [ "$EUID" -ne 0 ]; then
  echo "Error - we don't have root, which is required to update your printer configuration."
  echo "Please run this script with sudo ./printer.sh"
  exit 1
fi
clear
echo "This script installs all ISTA shared printers - if you continue,"
echo "any previously-installed printer configurations will be removed."
echo ""
if enter_yesno "Are you sure you want to continue?" ; then
	echo ""
	echo "- looking for curl..."
	if ! command -v curl > /dev/null; then
		echo "- curl not found, installing..."
		apt-get update
		apt-get install curl -y
		echo ""
	fi
	echo "Ok!"
	echo ""
	echo "- downloading new printers.conf"
	curl -L -o cupsjammy23.tar.gz https://seafile.ist.ac.at/f/67948aaebfbb47438b27/?dl=1  > /dev/null 2>&1 || die "curl -L -o cupsjammy23.tar.gz cupsjammy23.tar.gz https://seafile.ist.ac.at/f/67948aaebfbb47438b27/?dl=1 "
	echo "- stopping CUPS..."
	systemctl stop cups
	echo "- backing up old printers.conf(*)"
	mv /etc/cups/printers.conf /etc/cups/printers.conf.old
	echo "- unpacking new printers.conf"
	tar -C /etc/ -xf ./cupsjammy23.tar.gz || die "tar -C /etc/cups/ -xvf ./cupsjammy23.tar.gz"
	sed -i 's/Option print-color-mode monochrome/Option print-color-mode color/g' /etc/cups/printers.conf || die "failed to update printers.conf to allow colour prints"
	echo "- testing config"
	test -r /etc/cups/printers.conf && test -w /etc/cups/printers.conf || die "After extracting common/cupsjammy23.tar.gz, file /etc/cups/printers.conf does not exist or is not readable/writable"
	echo "- config looks good - restarting CUPS"
	systemctl start cups || die "systemctl start cups"
	echo "- cleaning up..."
	rm cupsjammy23.tar.gz
	echo ""
	echo "Finished - all ISTA printers have now been installed."
	echo ""
	echo "(* NB: if you accidentally removed a printer that you need later,"
	echo "its configuration can be found in /etc/cups/printers.conf.old )"
	echo ""
else
	echo ""
	echo "Ok, exiting..."
	echo ""
	echo "Here's a hint - you can backup your /etc/cups folder"
	echo "especially the file /etc/cups/printers.conf which contains"
	echo "all of your current printer configs."
	echo ""
	echo "(This script attempts to do this automatically, but it would"
	echo "be a good idea to back it up yourself first!)"
	echo ""
	exit
fi
