# cups

This script simply installs all shared ISTA printers on campus using sensible names. You need to make it executable:
sudo chmod +x printer.sh
and then run it with 
sudo ./printer.sh 

Note that it will remove all existing printers installed on your system, and back up their configuration to /etc/cups/printers.conf.old

After running it, all printers should be available on your system: feel free to delete unwanted printers and set a default printer in Settings > Printers

For more help, contact IT Support it@ist.ac.at