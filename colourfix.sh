#!/bin/bash
echo "Re-enabling colour printing for all printers..."
function stopcups {
	while true; do
		systemctl stop cups
		if [ $? -eq 0 ]; then
			echo "Stopped cups service"
			break
		else
			echo "Failed to stop cups, retrying in 2 seconds"
			sleep 2
		fi
		done
}

function startcups {
	max_attempts=5
	count=0
	while [ $count -lt $max_attempts ]; do
		systemctl start cups
		if [ $? -eq 0 ]; then
			echo "Started cups service"
			break
		else
			echo "Failed to start cups, retrying in 2 seconds"
			sleep 2
		fi
	done

	if [ $count -eq $max_attempts ]; then
		echo
		echo "WARNING: Starting cups has failed five times and is now being skipped:"
		echo "printing will not work until this is resolved. Please contact IT Support"
		echo "via it@ist.ac.at"
		echo
		exit 1
	fi
}

function fixcolour {
  sed -i 's/Option print-color-mode monochrome/Option print-color-mode color/g' /etc/cups/printers.conf
}

if [ "$EUID" -ne 0 ]; then
  echo "Error - we don't have root, which is required to update your printer configuration."
  echo "Please run this script with sudo ./colourfix.sh"
  exit 1
fi

stopcups
fixcolour
startcups

echo "All done"
